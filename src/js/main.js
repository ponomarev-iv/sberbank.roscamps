var Utils = {
    "digitsInString": function (str) {
        return str.replace(/\D+/g, '');
    },
    "bitNumber": function (digit, additionalChar) {
        additionalChar = additionalChar || "&nbsp;";

        return digit.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1' + additionalChar);
    },
    "validateOnlyDigitsInput": function (event) {
        var key = window.event ? event.keyCode : event.which;
        var prevent = true;

        if (event.keyCode === 8 || event.keyCode === 46) {
            prevent = false;
        } else {
            prevent = key < 48 || key > 57
        }

        return prevent;
    },
    "capitalizeFirstLetter": function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    },
    "createDateFromStr": function (str) {
        var date_str = str.split('.');
        var day_str = date_str[0];
        var month_str = date_str[1];
        var year_str = date_str[2];
        var date_iso_str = year_str + '-' + month_str + '-' + day_str;
        //date_iso_str = Date.parse(date_iso_str);
        date_iso_str = new Date(date_iso_str);
        return date_iso_str;
    },
    "getDateDiffInDays": function (startDate, endDate) {
        return (endDate.getTime() - startDate.getTime()) / (1000*60*60*24) + 1;
    },
    "declOfNum": function(number, titles) {
        var cases = [2, 0, 1, 1, 1, 2];
        return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}
};

var nowDate = new Date();

function getDatesFormPeriodsSelect(dateStr) {
    var periodDates = dateStr.split('-');
    var startDate = Utils.createDateFromStr(periodDates[0]);
    var endDate = Utils.createDateFromStr(periodDates[1]);

    return{
        'start': startDate,
        'end': endDate
    }
}

function setCurrentPeriodDays(days) {
    $('#offer-data-days').text(days +' '+Utils.declOfNum(days,['день','дня','дней']));
}

function prepareOfferPopup(camp, plhs, booking) {

    var cid = camp.attr('id');

    function collectData(el) {
        var dataTexts = [];
        el.find('.camp-data').children('.camp-data__item').each(function () {
            dataTexts.push($(this).text());
        });
        return {
            "geo": el.find('.camp-geo').eq(0).text(),
            "pic": el.find('.camp__gal').children('img:first-of-type').attr('src'),
            "title": el.find('.camp-title').text(),
            "data": dataTexts,
            "cost": Utils.digitsInString(el.find('.js-offer-cost').text())
        }
    }

    function setData(plhs, data) {
        plhs.geo.text(data.geo);
        plhs.pic.attr('src', data.pic);
        plhs.title.text(data.title);
        plhs.lname.attr('value', data.title);
        plhs.data.find('.js-offer-data-item').each(function (i) {
            $(this).text(data.data[i])
        });
        plhs.cost.html(Utils.bitNumber(data.cost) + ' руб.');
        plhs.cost.data('def-cost', data.cost);

        var periodOpt;
        var disableOpt = '';
        var periodDates;

        plhs.period.empty();

        data.period[0].periods.forEach(function (item) {

            $.each(item, function (key, val) {
                periodDates = getDatesFormPeriodsSelect(key);

                if(nowDate > periodDates.start){
                    disableOpt = 'disabled';
                }else{
                    disableOpt = '';
                }

                periodOpt = $('<option '+disableOpt+' value="' + key + '">' + key + '</option>');

                if (val.length) {
                    periodOpt.data('cost', val);
                }
                plhs.period.append(periodOpt);
            });
        });

        periodDates = getDatesFormPeriodsSelect(plhs.period.val());
        var datesDiff = Utils.getDateDiffInDays(periodDates.start,periodDates.end);
        setCurrentPeriodDays(datesDiff);

        var costTerms = camp.find('.camp-cost-hint');

        if (costTerms.length) {
            createTermsRow(plhs.period, costTerms);
            booking.costTermHandler();
        }
    }

    function createTermsRow(el, terms) {
        var newRow = $('<div id="cost-terms-row" class="form-row"></div>');
        newRow.append($('<div class="form-row__item komputerra"></div>'));

        var costTxt;
        var termTxt;
        var cost;
        var newTerm;
        var checked = '';

        newRow.insertAfter(el.parents('.form-row'));

        terms.find('p').each(function (i) {
            if (!i) {
                checked = 'checked';
            } else {
                checked = '';
            }
            costTxt = $(this).children('b');
            cost = Utils.digitsInString(costTxt.text());
            costTxt.remove();
            termTxt = $(this).text().replace(/\. —/g, '').trim();

            newTerm = $('<label class="lbl-row"><div class="lbl-row__inp"><div class="fake-check fake-check_radio"> <input class="js-cost-term" name="type" type="radio" data-term-cost="' + cost + '" value="' + termTxt + '" ' + checked + '><div class="fake-check__inp"></div></div></div><span class="lbl-row__txt">' + Utils.capitalizeFirstLetter(termTxt) + '</span></label>');

            newRow.children(':first-child').append(newTerm);
        });
    }

    var jqxhr = $.getJSON("data/camps-periods.json", function () {
        //console.log( "success" );
    })
        .done(function (data) {
            var plhData = collectData(camp);

            plhData.period = data.items.filter(function (item) {
                if (item.id === cid) {
                    return item;
                }
            });

            setData(plhs, plhData);

            $.magnificPopup.open({
                items: {
                    src: '#mfp-booking'
                },
                type: 'inline',
                callbacks: {
                    close: function () {
                        //booking.removeCostTerms();
                        booking.formReset();
                    }
                }
            });
        })
        .fail(function () {
            alert('Извините, данные не доступны :-( Пожалуйста попробуйте позже.')
        })
        .always(function () {
            //console.log( "complete" );
        });
}

function BookingFormEvents(form, plhs) {
    if (!form.length) {
        throw new Error('booking form not found');
    }

    var fn = this;

    var childsNum = $('#child-num');
    var costFor = $('#offer-cost-for');
    var defCost;
    var periodCost;

    var periodDates;
    var datesDiff;

    plhs.period.on('change', function () {
        var optionSelected = $("option:selected", this);
        periodCost = optionSelected.data('cost');

        if (periodCost) {
            plhs.cost.data('def-cost', periodCost);
            childsCalc(childsNum);
        }

        periodDates = getDatesFormPeriodsSelect($(this).val());
        datesDiff = Utils.getDateDiffInDays(periodDates.start,periodDates.end);
        setCurrentPeriodDays(datesDiff);
    });

    childsNum.on('change', function (e) {
        childsCalc($(this));
    });

    /*childsNum.on('keypress', function (e) {

        if (Utils.validateOnlyDigitsInput(e)) {
            e.returnValue = false;
            e.preventDefault();
        }

        childsCalc($(this));
    });*/

    function childsCalc(field) {
        var val = field.val();

        if (val) {
            defCost = plhs.cost.data('def-cost');
            setTotalCost(plhs.cost, costFor, calcTotal(val, defCost), val)
        }
    }

    function calcTotal(num, cost) {
        return Utils.bitNumber(cost * num);
    }

    function setTotalCost(elCost, elCostFor, cost, num) {
        elCost.html(Utils.bitNumber(cost) + ' руб.');

        elCostFor.children('span').remove();

        if (num > 1) {
            elCostFor.append('<span> за ' + num + '-х детей</span>');
        }
    }

    fn.costTermHandler = function () {
        var costTerms = $('.js-cost-term');

        if (costTerms.length) {
            costTerms.on('change', function () {
                var $this = $(this);
                if ($this.is(':checked')) {
                    plhs.cost.data('def-cost', $this.data('term-cost'));
                    childsCalc(childsNum);
                }
            })
        }
    };

    fn.formReset = function () {
        form[0].reset();
        $('#cost-terms-row').remove();
        $('#offer-cost-for > span').remove();
    };

    fn.removeCostTerms = function () {
        $('#cost-terms-row').remove();
    };
}

function ExpandCampDescription(camp, btn) {
    var fn = this;
    var desc = camp.find('.camp-extended');

    fn.openState = function (btn,camp,desc) {
        btn.text('Скрыть');
        btn.addClass('is-toggle');
        camp.addClass('is-expanded');

        $('html, body').animate({
            scrollTop: desc.offset().top
        }, 300);
    };

    fn.closeState = function (btn,camp) {
        btn.text('Подробнее');
        btn.removeClass('is-toggle');
        camp.removeClass('is-expanded');
    };

    var curExtended = $('.camp.is-expanded').not(camp);
    var curDesc = curExtended.find('.camp-extended');

    curDesc.slideUp(300, function () {
        var exBtn = curExtended.find('.is-toggle');
        fn.closeState(exBtn,curExtended);
    });

    desc.slideToggle(300, function () {
        if ($(this).is(':visible')) {
            fn.openState(btn,camp,$(this));
        } else {
            fn.closeState(btn,camp)
        }
    });
}

function getCamp(el) {
    return el.parents('.camp');
}

function sendForm(form, url, booking) {
    var data = form.serialize();

    $.ajax({
        method: "POST",
        url: 'http://sberbank.roscamps.ru/' + url,
        data: data
    }).done(function (msg) {
        var valdata = JSON.parse(msg);

        if (!valdata.valid) {
            alert('Пожалуйста убедитесь что все поля формы заполнены правильно.')
        } else {
            $.magnificPopup.close();
            showSendSuccessMes(form.data('type'));
            booking.formReset();
            //B!!!!
            form[0].reset();
        }
    });

}

function showSendSuccessMes(type) {
    if(!type){
        throw new Error('message type undefined');
    }

    var mes = $('#mfp-success');

    var mesTxt = {
        "question": {
            "title": "Спасибо за&nbsp;обращение!",
            "txt": "Ваше обращение отправлено! Наши специалисты свяжутся с&nbsp;Вами в&nbsp;ближайшее время"
        },
        "request": {
            "title": "Ваша заявка успешно отправлена!",
            "txt": "В&nbsp;ближайшее время с&nbsp;Вами свяжутся наши&nbsp;специалисты для&nbsp;уточнения деталей заказа"
        }
    };

    mes.find('.popup__title').html(mesTxt[type].title);
    mes.find('.popup__success').html(mesTxt[type].txt);

    $.magnificPopup.open({
        items: {
            src: mes
        },
        type: 'inline'
    });
}

function initMagnificPopup(params) {

    var selector = $(params.selector);

    if (selector.length) {
        selector.on('click', function (e) {
            e.stopPropagation();

            $(this).magnificPopup({
                type: 'inline'
            }).magnificPopup('open');

            e.preventDefault();
        });
    }

    var mfpClose = $('.js-mfp-close');

    mfpClose.on('click', function () {
        $.magnificPopup.close();
    })
}

function initPopupGallery(params) {
    var selector = $(params.selector);

    selector.each(function() {
        $(this).magnificPopup({
            delegate: 'img',
            type: 'image',
            gallery: {
                enabled:true,
                tCounter: ''
            },
            callbacks:{
                /*elementParse: function(item) {
                    // Function will fire for each target element
                    // "item.el" is a target DOM element (if present)
                    // "item.src" is a source that you may modify

                    console.log('Parsing content. Item object that is being parsed:', item);
                },
                change: function() {
                    console.log('Content changed');
                    console.log(this.content); // Direct reference to your popup element
                },*/
                open: function() {
                    var obj = this;

                    obj.arrowLeft.on('click.galProxy', function () {
                        obj.ev.children('.small-gal-arrow-left').trigger('click');
                    });

                    obj.arrowRight.on('click.galProxy', function () {
                        obj.ev.children('.small-gal-arrow-right').trigger('click');
                    });

                    obj.content.on('click.galProxy', function () {
                        obj.ev.children('.small-gal-arrow-right').trigger('click');
                    });
                }
            }
        });
    });
}

function showHintsOnTouch() {
    var els = $('.js-touch-hint');

    els.on('touchend', function (e) {
        e.preventDefault();
        $(this).addClass('is-touch');

        $('body').on('touchend.closehint', function (e) {

            if (!$(e.target).closest(els).length) {
                els.removeClass('is-touch');
                $(this).off('touchend.closehint');
            }
        })
    });

    els.hover(function () {
        $(this).addClass('is-touch');
    },function () {
        $(this).removeClass('is-touch');
    })
}

function smallImageGallery() {
    var fn = this;
    var gallerys = $('.js-small-gal');
    var gal;
    var curImg;
    var lArrow;
    var rArrow;

    gallerys.each(function () {
        gal = $(this);
        curImg = gal.children('img').eq(0);
        curImg.addClass('is-cur');
        lArrow = gal.find('.small-gal-arrow-left');
        rArrow = gal.find('.small-gal-arrow-right');

        lArrow.on('click', function (e) {
            var $this = $(this);
            goLeft($this.parents('.js-small-gal'),$this);
        });

        rArrow.on('click', function (e) {
            var $this = $(this);
            goRight($this.parents('.js-small-gal'),$this);
        });
    });

    function goLeft(gal,arrow) {
        var cur = findCurrent(gal);

        var prevImg = cur.prev('img');

        if(!prevImg.length){
            prevImg = gal.children('img').eq(-1);
        }

        updateState(prevImg,gal);

        if(isLast('prev',cur) <= 1){
            arrow.addClass('is-last');
        }else{
            arrow.removeClass('is-last');
        }
    }

    function goRight(gal,arrow) {
        var cur = findCurrent(gal);

        var nextImg = cur.next('img');

        if(!nextImg.length){
            nextImg = gal.children('img').eq(0);
        }

        updateState(nextImg,gal);

        if(isLast('next',cur) <= 1){
            arrow.addClass('is-last');
        }else{
            arrow.removeClass('is-last');
        }
    }

    function findCurrent(gal) {
        return gal.find('img.is-cur');
    }

    function updateState(cur,gal) {
        cur
            .addClass('is-cur')
            .siblings('img')
            .removeClass('is-cur');

        gal.find('.is-last').removeClass('is-last');
    }

    function isLast(dir,el) {
        var isLast;

        if(dir === 'prev'){
            isLast = el.prevAll('img').length;
        }else{
            isLast = el.nextAll('img').length;
        }

        return isLast;
    }
}

$(document).ready(function () {
    var getOfferBtn = $('.js-get-offer');

    var offerReplacmentData = {
        "geo": $('#offer-geo'),
        "pic": $('#offer-pic'),
        "title": $('#offer-title'),
        "data": $('#offer-data'),
        "period": $('#offer-periods'),
        "cost": $('#offer-cost'),
        "lname": $('#lname')
    };

    var bookingForm = $('#form-booking');
    var booking = new BookingFormEvents(bookingForm, offerReplacmentData);

    getOfferBtn.on('click', function () {
        prepareOfferPopup(getCamp($(this)), offerReplacmentData, booking);
    });

    var expandCampBtn = $('.js-camp-expand');

    expandCampBtn.on('click', function () {
        var $this = $(this);
        var expandedDesc = new ExpandCampDescription(getCamp($this), $this);
    });

    $.validate({
        form: '#form-booking',
        lang: 'ru',
        showHelpOnFocus: false,
        addSuggestions: false,
        scrollToTopOnError: false,
        onError: function ($form) {
            //$form.find('button[name="send-booking"]').attr('disabled','');
            return false;
        },
        onSuccess: function ($form) {
            //$form.find('button[name="send-booking"]').removeAttr('disabled');
            sendForm($form, 'valid_request.php', booking);
            return false;
        }
    });

    $.validate({
        form: '#bot-contacts, #pop-contacts',
        lang: 'ru',
        showHelpOnFocus: false,
        addSuggestions: false,
        scrollToTopOnError: false,
        onError: function ($form) {
            return false;
        },
        onSuccess: function ($form) {
            sendForm($form, 'valid_question.php', booking);
            return false;
        }
    });

    initMagnificPopup({'selector': '.js-mfp'});

    smallImageGallery();

    initPopupGallery({'selector': '.js-mfp-gal'});

    showHintsOnTouch();
});

//sberbank.roscamps.ru
//sberbank-roscamps
//sberbank34Roscamps
//киногерой: Билл Хикок
