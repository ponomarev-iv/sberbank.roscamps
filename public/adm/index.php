<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/inc/conf.php");// 
function checkAdmin($login,$password, $in_sess = false){
	$ret_id = -1;
	
		$dbh = new MyPDO;
		
		$sth = $dbh->prepare("SELECT * FROM `T_ADMINS` WHERE LOGIN = :login ORDER BY `ID`");
		$ex = array(':login'=>$login);

		$sth->execute($ex);
		$resp = $sth->fetch(PDO::FETCH_ASSOC);

		$export_state = array();
		$project_types =array();

		/*if($resp !== false){
				if(md5($password) == $resp['PASSWORD'] || $in_sess){
						$ret = true;
				}
		}
*/
		return $resp['ID'];
}
function clear(){
	unset($_SESSION['auth']);
}

session_start();

if(isset($_REQUEST) && isset($_REQUEST['clear'])){
	clear();
}
$result = false;
$req_login = '';
if(isset($_REQUEST) && isset($_REQUEST['LOGIN'])){
	$req_login = $_REQUEST['LOGIN'];
	if(isset($_REQUEST['PASSWORD'])){
		$result = checkAdmin($_REQUEST['LOGIN'], $_REQUEST['PASSWORD']);
		$_SESSION['auth'] = $_REQUEST['LOGIN'];	
	}
} elseif(isset($_SESSION['auth'])) {
		$result = checkAdmin($_SESSION['auth'],'',true);
}

if($result == false):?>
	<html>
		<head>
			<meta charset="utf-8">
		</head>
		<body>
			<form action="/adm/" method="POST">
			<input type="text" name="LOGIN" value="<?=$req_login?>" placeholder="Логин">
			<input type="password" name="PASSWORD" placeholder="Пароль">
			<button type="submit">Войти</button>
			</form>
		</body>
	</html>
	
<? else :?>

<?
	$dbh = new MyPDO;
	$sth = $dbh->prepare("SELECT * FROM T_REQUEST ORDER BY ID");
	$sth->execute(/*$datearr*/);
	$pt = $sth->fetchAll(PDO::FETCH_ASSOC);
	$restable = array();
	foreach ($pt as $v){
			$restable[] = $v;
	}

	$tableData = '';

	foreach ($restable as $row){
	$tableData .= '<tr>';
	$tableData .= '<td>'.$row["ID"].'</td>';
	$tableData .= '<td>'.$row["INITIALS"].'</td>';
	$tableData .= '<td>'.$row["MAIL"].'</td>';
	$tableData .= '<td>'.$row["PHONE"].'</td>';
	$tableData .= '<td>'.$row["PERIOD"].'</td>';
	$tableData .= '<td>'.$row["TYPE"].'</td>';
	$tableData .= '<td>'.$row["LNAME"].'</td>';

	$tableData .= '</tr>';
	}
	?>
	<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <style type="text/css">
		*{
			text-align: left;
		}
		th, td{
			min-width: 80px;
			padding-left: 20px;
		}

		.content,
		table{
			max-width: 90% !important;
			margin: 40px auto 0;
		}
	</style>
</head>
<h1>Заявки</h1>
<table class="table table-striped" id="requests" style="text-align: center; width: 100%">
<thead class="thead-dark">
	<tr>
		<th>ID</th>
		<th>ФИО</th>
		<th>ПОЧТА</th>
		<th>ТЕЛЕФОН</th>
		<th>СМЕНА</th>
		<th>ПРОЖИВАНИЕ (для Компьютерия)</th>
		<th>ЛАГЕРЬ</th>
	</tr>
	</thead>
	<tbody>
		<?php echo $tableData;?>	
	</tbody>
</table>

	<?
	$dbh = new MyPDO;
	$sth = $dbh->prepare("SELECT * FROM T_QUESTIONS ORDER BY ID");
	$sth->execute(/*$datearr*/);
	$pt = $sth->fetchAll(PDO::FETCH_ASSOC);
	$restable = array();
	foreach ($pt as $v){
			$restable[] = $v;
	}

	$tableData = '';

	foreach ($restable as $row){
	$tableData .= '<tr>';
	$tableData .= '<td>'.$row["ID"].'</td>';
	$tableData .= '<td>'.$row["INITIALS"].'</td>';
	$tableData .= '<td>'.$row["MAIL"].'</td>';
	$tableData .= '<td>'.$row["PHONE"].'</td>';
	$tableData .= '<td>'.$row["MESSAGE"].'</td>';
	//$tableData .= '<td>'.$row["LNAME"].'</td>';

	$tableData .= '</tr>';
	}

	?>

<div class="content">
		<div class="row">
			<a href="#"  class="btn btn-success" onclick="tblToArr('requests'); return false;">Импорт</a>
		</div>
	</div>

<br>
<br>

<h1>ВОПРОСЫ</h1>

<table class="table table-striped" id="questions" style="text-align: center; width: 100%">
	<thead class="thead-dark">
	<tr>
		<th>ID</th>
		<th>ФИО</th>
		<th>ПОЧТА</th>
		<th>ТЕЛЕФОН</th>
		<th>ТЕКСТ СООБЩЕНИЯ</th>
	</tr>
	</thead>
	<tbody>
		<?php echo $tableData;?>	
	</tbody>
</table>

<div class="content">
		<div class="row">
			<a href="#" class="btn btn-success" onclick="tblToArr('questions'); return false;">Импорт</a>
		</div>
	</div>
    <br>
    <br>
<?endif;?>

<script src="/adm/jquery-3.2.1.min.js"></script>

<script>	

		function tblToArr(tbl_id){

		ARR = [];
		//главная

		$('table#'+tbl_id+' thead tr').each(function(i){

		   var tr = [];
		   var that = this;
		   $(that).find('th').each(function(j){
		   		if(!$(this).hasClass('noparse')){
			        tr.push($(this).text());
			        //console.log($(this));
		   		}
		   })

		   ARR.push(tr);
		});
		$('table#'+tbl_id+' tbody tr').each(function(i){

		   var tr = [];
		   var that = this;
		   $(that).find('td').each(function(j){

		   		if(!$(this).hasClass('noparse')){

						tr.push($(this).text().replace(/;/g,','));					

					}
		       //console.log($(this));
		   })

		   ARR.push(tr);
		});
		var tr=[];
		tr.push('');
		ARR.push(tr);	

		var tr=[];
		tr.push('');
		ARR.push(tr);

		csvHead = "data:text/csv;charset=utf-8,\uFEFF";
		//csvHead = "data:text/csv;charset=utf-8,";
		csvContent = "";
		ARR.forEach(function(infoArray, index){

		   dataString = infoArray.join(";");
		   csvContent += index < ARR.length ? dataString+ "%0A" : dataString;
		}); 

		//var csvname = $('#csvname').val()

		var csvname = tbl_id;
		var encodedUri = encodeURI(csvContent);
		csvContent = csvHead + csvContent;	

		var link = document.createElement("a");
		link.setAttribute("href", csvContent);
		link.setAttribute("download", csvname+'.csv');
      document.body.appendChild(link);  
		link.click();
	}
</script>
</body>
</html>