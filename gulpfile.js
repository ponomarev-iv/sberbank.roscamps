const paths = {
    gulp: './gulp',
    src:{
        data: 'src/data/',
        buildData: 'src/data/temp/',
        js: 'src/js/',
        css: 'src/css/',
        img: 'src/img/'
    },
    pug: {
        tmpl: 'src/tmpl/',
        inc: 'src/includes/',
        pages: 'src/pages/',
        popup: 'src/popup/',
        base: './'
    },
    public:{
        html: 'public/',
        css: 'public/css/',
        img: 'public/img/',
        js: 'public/js/'
    }
};
//test
const gulp = require('gulp');
const path = require('path');
const watch = require('gulp-watch');
//const imagemin = require('gulp-imagemin');
const plugins = require('gulp-load-plugins')();

function getTask(task) {
    return require(`./gulp/${task}`)(gulp, plugins, paths, path);
}

gulp.task('pug-data', getTask('pug-data'));
gulp.task('pug-build' ,getTask('pug-build'));
gulp.task('style-build', getTask('style-build'));
gulp.task('scss-build', getTask('scss-build'));
gulp.task('img-min', getTask('img-min'));
gulp.task('js-min', getTask('js-min'));

gulp.task('default',['pug-build','style-build','watch']);

gulp.task('watch', function () {
    watch(`${paths.src.data}*.json`, function () {
        gulp.start('pug-data');
    });
    watch(`${paths.src.buildData}*.json`, function () {
        gulp.start('pug-build');
    });
    watch(`${paths.pug.tmpl}**/*.pug`, function () {
        gulp.start('pug-build')
    });
    watch([`${paths.pug.pages}*.pug`], function () {
        gulp.start('pug-build')
    });
    watch([`${paths.pug.inc}*.pug`], function () {
        gulp.start('pug-build')
    });
    watch(`${paths.src.css}*.pcss`, function () {
        gulp.start('style-build');
    });
    watch(`${paths.src.css}magnific-popup/*.scss`, function () {
        gulp.start('scss-build');
    });
    watch(`${paths.src.img}**/*`, function () {
        gulp.start('img-min');
    });
    watch(`${paths.src.js}*.js`, function () {
        gulp.start('js-min');
    });
});
