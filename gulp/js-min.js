module.exports = function (gulp, plugins, paths, path) {
    const jsminify = require('gulp-minify');

    return function () {
        gulp.src([
            `${paths.src.js}main.js`
        ])
        .pipe(jsminify({
            ext:{
                min:'.min.js'
            },
            ignoreFiles: ['.min.js']
        }))
        .pipe(gulp.dest(paths.public.js));
    };
};