module.exports = function (gulp, plugins, paths, path) {
    const cssnano = require('cssnano');
    const mqpacker = require('css-mqpacker');
    const next = require('postcss-cssnext');
    return function () {
        gulp.src(`${paths.src.css}magnific-popup/*.scss`)
            .pipe(plugins.sass.sync().on('error', plugins.sass.logError))
            .pipe(plugins.postcss([
                next(),
                mqpacker({
                    sort: true
                }),
                cssnano()
            ]))
            .pipe(plugins.rename({
                extname: ".css"
            }))
            .pipe(gulp.dest(paths.public.css));
    };

};