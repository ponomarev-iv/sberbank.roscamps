module.exports = function (gulp, plugins, paths, path) {
    const atImport = require('postcss-import');
    const partialImport = require('postcss-partial-import');
    const cssnano = require('cssnano');
    const normalize = require('postcss-normalize');
    const next = require('postcss-cssnext');
    const nested = require('postcss-nested');
    const mqpacker = require('css-mqpacker');
    //const responsiveType = require('postcss-responsive-type');

    function swallowError (error) {

        // If you want details of the error in the console
        console.log(error.toString());

        this.emit('end')
    }

    return function () {
        gulp.src(`${paths.src.css}style.pcss`)
            .pipe(plugins.postcss([
                normalize(),
                atImport(),
                next(),
                // partialImport({
                //     path: [`${paths.src.css}`]
                // })
                nested(),
                mqpacker({
                    sort: true
                }),
                //responsiveType(),
                cssnano()
            ]))
            .pipe(plugins.rename({
                extname: ".css"
            }))
            .on('error', swallowError)
            .pipe(gulp.dest(paths.public.css));
    };
};