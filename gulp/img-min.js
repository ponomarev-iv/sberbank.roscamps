module.exports = function (gulp, plugins, paths, path) {
    //const imagemin = require('gulp-imagemin');

    return function () {
        gulp.src(`${paths.src.img}**/*`)
            .pipe(plugins.imagemin([
                plugins.imagemin.svgo({
                    plugins: [
                        {removeViewBox: false},
                        {cleanupIDs: false},
                        {minifyStyles: true}
                    ]
                })
            ]))
            .pipe(gulp.dest(paths.public.img));
    };
};