module.exports = function (gulp, plugins, paths, path) {
    const fs = require('fs');
    // let file = `${paths.pug.buildData}data.json`;
    // if(file){
    //     const j = fs.readFileSync(file,'utf-8');
    //     console.log(j);
    // }else{
    //     console.log('e');
    // }
    function swallowError (error) {

        // If you want details of the error in the console
        console.log(error.toString());

        this.emit('end')
    }

    return function () {
        gulp.src([`${paths.pug.pages}*.pug`])
            .pipe(plugins.data(function() {
                let file = `${paths.src.buildData}data.json`;

                let json = fs.readFileSync(file,'utf-8');

                return JSON.parse(json);
            }))
            .pipe(plugins.pug({
                pretty: true,
                basedir: './'
            }))
            .on('error', swallowError)
            .pipe(gulp.dest(paths.public.html))
    };
};